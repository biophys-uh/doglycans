![Scheme](doc/front.jpg)

# *doGlycans* Tool

## *doGlycans* free software

The *doGlycans* tool set is a free software package and can be redistributed and/or modified under
the terms of the GNU General Public License as published by the Free Software Foundation,
version 3 of the License. For more details, see 

https://www.gnu.org/licenses/gpl-3.0.en.html		

The *doGlycans* source code can be downloaded from 

https://bitbucket.org/biophys-uh/doglycans.git


## *doGlycans* is doglycans

In this manual, the doglycans as a command is given in lowercase. Therefore, on a command
level, “*doGlycans*” will be and should be given as “doglycans”.

For more information on the research activities of the team standing behind the tool, please visit the
webpage:

https://www.helsinki.fi/en/researchgroups/biophysics	

## Disclaimer

The programs and scripts provided in the *doGlycans* package are 'as-is', without any express or
implied warranty. In no event will the authors be held liable for any damages arising from the use of
this software. The *doGlycans* tool and the manual will be updated if any additional features are
added to the tool. The users are requested to always use the latest version of the tool. Any
comments or issues in the usage of the tool can be posted by creating an issue in the *doGlycans*
public repository:

https://bitbucket.org/biophys-uh/doglycans/

## Citing *doGlycans*

Reinis Danne, Chetan Poojari, Hector Martinez-Seara, Sami Rissanen, Fabio Lolicato, Tomasz
Róg, Ilpo Vattulainen. *doGlycans* – Tools for Preparing Carbohydrate Structures for Atomistic
Simulations of Glycoproteins, Glycolipids, and Carbohydrate Polymers for GROMACS. J. Chem. Inf.
Model. (2017) [Please update the reference not known in full at the time of writing this manual].

# Installation

## Dependencies

Make sure that following packages are available

- 3.6 ⩽ Python ⩽ 3.12
  -	`numpy`
  -	`ply` ⩾ 3.10
- OpenBabel 3.1 or 2.4 with Python API


 ### Using Conda

```sh
$ conda create --name doglycans "python==3.9.*" "numpy" "ply>=3.10"
$ conda activate doglycans
(doglycans) $ conda install -c conda-forge "openbabel"
```

### On Ubuntu/Debian without Conda

```sh
$ sudo apt install python3-pip python3-numpy python3-ply openbabel python3-openbabel
$ python -m venv ~/.venv
$ source ~/.venv/bin/activate
```

Installation of Python packages without a virtual environment is no longer possible with recent Linux distributions. 

## Download and install

After activation of Conda environment or Python virtual environment

```sh
$ git clone https://gitlab.com/doglycans/doglycans
$ cd doglycans
$ python -m pip install .
```

Three commands should be available
- `doglycans`
- `prepreader`
- `doglycans2charmmgui`

## Optional dependencies

- GROMACS

As the *doGlycans* tool generates input files for simulations to be carried out in the GROMACS
molecular dynamics (MD) simulation package, for taking advantage of its functionalities the users
should have the GROMACS package installed.

Some GROMACS versions might require minor changes to the `.mdp` files.

# Development

One way to setup development environment is following:
- Clone the project and open it in VSCode.
- Install _Dev Containers_ extension and call _Dev Containers: Reopen in Container_.
- To see protein structures open them from the outside of the VSCode, using PyMol or VMD.

## Editable installation

With normal installation, the package is copied and changes to the files are not considered.
This is why during development you should use editable installation.

```sh
python -m pip install -e .
```

## Automated tests

```sh
$ conda activate doglycans
(doglycans) $ python -m unittest discover -s test
(doglycans) $ bash test.sh glycam06h
(doglycans) $ bash test.sh glycam06j
(doglycans) $ bash test.sh glycam06j-mod
```

Additionally, GitLab CI scripts run the tests after every change. Multiple Python versions and OpenBabel versions are tested.
