import unittest

# This can be simplified after droping of OpenBabel 2.4.1
try:
    from openbabel import openbabel as ob
except ImportError:
    import openbabel as ob


from doglycans.prepreader import readPrepFile


class TestReadPrepFile(unittest.TestCase):
    def test_readPrepFile(self):
        fileName = "./test/GLYCAM_06h_4GB.prep"

        with open(fileName, 'r') as f:
            residues = readPrepFile(f, no_warn=True)
            exampleResidue = residues["4GB"]

            converter = ob.OBConversion()
            converter.SetOutFormat("pdb")
            output = converter.WriteString(exampleResidue)

            assert("UNL" not in output)


if __name__ == "__main__":
    TestReadPrepFile().test_readPrepFile()
