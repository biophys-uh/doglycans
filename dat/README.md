# GLYCAM force-field parameters

Files in this directory have been downloaded from [GLYCAM page](https://glycam.org/docs/forcefield/all-parameters/index.html) with an exception of `With_special_cases` files.

Files with special cases have been manually patched with:
- _frcmod.UnsaturatedRing-ProtonatedUA_  
  Extra parameters used to generate GROMACS `.itp` file.
- _GlcNH2_and_GlcHN3_
  > New nomenclature adopted to include protonated and unprotonated residues: 0YN - Alpha-D-Glucosamine (Alpha-D-GlcNH2) 0Yn - Beta-D-Glucosamine (Beta-D-GlcNH2) 0YNP - Protonated Alpha-D-Glucosamine (Alpha-D-GlcNH3+) 0YnP - Protonated Beta-D-Glucosamine (Beta-D-GlcNH3+) The first character specifies linkage position. The second character specifies the GLYCAM residue name in accordance with the GLYCAM one-letter code. The N/n third character is used here to indicate that instead of acetyl group as indicated by the letter Y, this residue is Glucosamine. The upper and lower case indicate alpha and beta. The fourth character has been included to allow naming the protonated residues.
- _4,5Unsaturated_and_Protonated_UA_
  > New nomenclature adopted to include the following residues: 045 - Termina 4,5-unsaturated uronate 245 - Terminal 4,5-unsaturated uronate with open valence at 2-O position 0ZBP - Protonated Beta-D-Glucuronic acid YuAP - Protonated Alpha-L-Iduronic acid The fourth character has been included to allow naming the protonated residues. 
from the same page.
