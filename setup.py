from setuptools import setup


def get_readme():
    with open("README.md", "r", encoding="utf-8") as fp:
        return fp.read()


setup(
    name="doglycans",
    description="Tools for preparing carbohydrate structures for atomistic simulations of glycoproteins, glycolipids, and carbohydrate polymers for GROMACS",
    url="https://bitbucket.org/biophys-uh/doglycans",
    long_description=get_readme(),
    long_description_content_type="text/markdown",
    install_requires=['numpy', 'ply'],

    packages=['doglycans'],

    entry_points={
        'console_scripts': [
            'doglycans=doglycans.doglycans:doglycans_cli',
            'prepreader=doglycans.prepreader:prepreader_cli',
            'doglycans2charmmgui=doglycans.doglycans2charmmgui:doglycans2charmmgui_cli'
        ]
    },
)
