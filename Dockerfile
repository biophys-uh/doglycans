FROM ubuntu:22.04

# https://serverfault.com/a/797318
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y
RUN apt-get upgrade -y

RUN apt-get install -y wget git


#
# Create a user
#

RUN useradd -m user

RUN apt-get install -y sudo
RUN echo "user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers

USER user
WORKDIR /home/user
ENV PATH="${PATH}:/home/user/.local/bin"

#
# Install Conda
#

# https://repo.anaconda.com/miniconda

RUN wget "https://repo.anaconda.com/miniconda/Miniconda3-py39_22.11.1-1-Linux-x86_64.sh" \
    && chmod +x ./Miniconda3-*-Linux-x86_64.sh \
    && bash ./Miniconda3-*-Linux-x86_64.sh -b -p /home/user/.conda \
    && rm ./Miniconda3-*-Linux-x86_64.sh

RUN eval "$(/home/user/.conda/bin/conda shell.bash hook)" \
    && conda init \
    && conda config --set auto_activate_base false


#
# Workarounds
#

USER root

# Needed by OpenBabel for some reason
RUN apt-get install -y libxrender1

# One of the files (doglycans.py) prints theta. Script fails without unicode.
RUN apt-get install -y locales && locale-gen en_US.UTF-8

USER user

ENV LANG "en_US.UTF-8"
ENV LC_ALL "en_US.UTF-8"


#
# Dependencies: Conda environment with Babel (doglycans)
#

ARG PYTHON_VERSION="3.9"
ARG OPENBABEL_VERSION="3.1.1"

RUN eval "$(/home/user/.conda/bin/conda shell.bash hook)" \
    && ( conda activate && conda create --quiet -y --name doglycans "python==${PYTHON_VERSION}.*" "numpy" "ply>=3.10" ) \
    && ( conda activate doglycans && conda install --quiet -y -c conda-forge "openbabel=${OPENBABEL_VERSION}" )
