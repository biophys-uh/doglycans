set -o nounset
set -e

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"
top_dir="${script_dir}"

function is_openbabel_v3()
{
    python -c "import sys ; import openbabel ; v3: bool = hasattr(openbabel, '__version__') and openbabel.__version__[0] == '3'; sys.exit(int(not v3))"

    return $?
}

if [ $# -eq 0 ]; then
    echo "Usage: test.sh [glycam06h|glycam06j|glycam06j-mod]"
    exit 1
fi

set -x

test_variant="$1"

if [ "${test_variant}" == "glycam06h" ]; then
    input_itp_file="glycam06h.itp"
    input_prep_file="GLYCAM_06h.prep"
elif [ "${test_variant}" == "glycam06j" ]; then
    input_itp_file="glycam06j.itp"
    input_prep_file="GLYCAM_06j-1.prep"
elif [ "${test_variant}" == "glycam06j-mod" ]; then
    input_itp_file="glycam06j-With_special-cases.itp"
    input_prep_file="GLYCAM_06j-1_With_special-cases.prep"
else
    echo "Unrecognized test variant ${test_variant}"
    exit 1
fi

rm -rf "${top_dir}/tmp/1_${test_variant}"
mkdir -p "${top_dir}/tmp/1_${test_variant}"

# Build Celulose molecule according to specified sequence

(
    # AMBER

    cd "${top_dir}/tmp/1_${test_variant}"

    prepreader -f "${top_dir}/dat/${input_itp_file}" -p "${top_dir}/dat/${input_prep_file}" -s "${top_dir}/Examples/CELLULOSE/sequence_file.seq" -c POL.pdb -o POL.itp --amber

    diff <(awk '{print $1, $2, $3, $4, $5, $6, $10, $11, $12}' POL.pdb | grep -v "^AUTHOR") <(awk '{print $1, $2, $3, $4, $5, $6, $10, $11, $12}' "${top_dir}/Examples/CELLULOSE/AMBERff/POL.pdb" | grep -v "^AUTHOR")
    diff POL.itp "${top_dir}/Examples/CELLULOSE/AMBERff/POL.itp"
)

rm -rf "${top_dir}/tmp/2_${test_variant}"
mkdir -p "${top_dir}/tmp/2_${test_variant}"

(
    # CHARMM + suppress warnings

    cd "${top_dir}/tmp/2_${test_variant}"

    prepreader -W -f "${top_dir}/dat/${input_itp_file}" -p "${top_dir}/dat/${input_prep_file}" -s "${top_dir}/Examples/CELLULOSE/sequence_file.seq" -c POL.pdb -o POL.itp --charmm

    diff <(awk '{print $1, $2, $3, $4, $5, $6, $10, $11, $12}' POL.pdb | grep -v "^AUTHOR") <(awk '{print $1, $2, $3, $4, $5, $6, $10, $11, $12}' ${top_dir}/Examples/CELLULOSE/CHARMMff/POL.pdb | grep -v "^AUTHOR")
    diff <(awk '{print $1, $2, $3, $4, $5, $6, $10, $11, $12}' POL_charmm-gui.pdb | grep -v "^AUTHOR") <(awk '{print $1, $2, $3, $4, $5, $6, $10, $11, $12}' ${top_dir}/Examples/CELLULOSE/CHARMMff/POL_charmm-gui.pdb | grep -v "^AUTHOR")
    diff POL.itp "${top_dir}/Examples/CELLULOSE/CHARMMff/POL.itp"
)

rm -rf "${top_dir}/tmp/3_${test_variant}"
mkdir -p "${top_dir}/tmp/3_${test_variant}"

(
    # OPLS (+ generate monomers)

    cd "${top_dir}/tmp/3_${test_variant}"

    prepreader -m -f "${top_dir}/dat/${input_itp_file}" -p "${top_dir}/dat/${input_prep_file}" -s "${top_dir}/Examples/CELLULOSE/sequence_file.seq" -c POL.pdb -o POL.itp --opls

    diff <(awk '{print $1, $2, $3, $4, $5, $6, $10, $11, $12}' POL.pdb | grep -v "^AUTHOR") <(awk '{print $1, $2, $3, $4, $5, $6, $10, $11, $12}' "${top_dir}/Examples/CELLULOSE/OPLS_AAff/POL.pdb" | grep -v "^AUTHOR")
    diff POL.itp "${top_dir}/Examples/CELLULOSE/OPLS_AAff/POL.itp"

    if [ "${test_variant}" == "glycam06h" ]; then
        # Expecting 3039 monomers (19s)
        [ $(ls -l . | wc -l) -eq 3039 ]
    elif [ "${test_variant}" == "glycam06j" ]; then
        [ $(ls -l . | wc -l) -eq 3105 ]
    elif [ "${test_variant}" == "glycam06j-mod" ]; then
        [ $(ls -l . | wc -l) -eq 3105 ]
    else
        exit 1
    fi

    # Skipping dimers and trimers because generation is slow
)

#
# Add glycans to protein
#

rm -rf "${top_dir}/tmp/4_${test_variant}"
mkdir -p "${top_dir}/tmp/4_${test_variant}"

(
    # OPLS-AA

    cd "${top_dir}/tmp/4_${test_variant}"

    doglycans -f "${top_dir}/dat/${input_itp_file}" -p "${top_dir}/dat/${input_prep_file}" -s "${top_dir}/Examples/CD59/sequence_file.seq" -c "${top_dir}/Examples/CD59/OPLS_AAff/ProtH.pdb" -i "${top_dir}/Examples/CD59/OPLS_AAff/ProtH.itp" -o ProtG.pdb -t ProtG.itp -B --opls

    cat <(head -n 3 ProtG.pdb) <(tail -n +5 ProtG.pdb) | grep -v "^AUTHOR" > ProtG_skip_header.pdb
    cat <(head -n 3 ${top_dir}/Examples/CD59/OPLS_AAff/ProtG.pdb) <(tail -n +5 ${top_dir}/Examples/CD59/OPLS_AAff/ProtG.pdb) | grep -v "^AUTHOR" > ProtG_skip_header_reference.pdb

    diff ProtG_skip_header.pdb ProtG_skip_header_reference.pdb
    diff ProtG.itp "${top_dir}/Examples/CD59/OPLS_AAff/ProtG.itp"
)

rm -rf "${top_dir}/tmp/5_${test_variant}"
mkdir -p "${top_dir}/tmp/5_${test_variant}"

(
    # CHARMM-FF

    cd "${top_dir}/tmp/5_${test_variant}"

    doglycans -f "${top_dir}/dat/${input_itp_file}" -p "${top_dir}/dat/${input_prep_file}" -s "${top_dir}/Examples/CD59/sequence_file.seq" -c "${top_dir}/Examples/CD59/CHARMMff/ProtH.pdb" -i "${top_dir}/Examples/CD59/CHARMMff/ProtH.itp" -o ProtG.pdb -t ProtG.itp -B --charmm ${top_dir}/dat/GLYCAM_06j_resname.txt

    cat <(head -n 3 ProtG.pdb) <(tail -n +5 ProtG.pdb) | grep -v "^AUTHOR" > ProtG_skip_header.pdb
    cat <(head -n 3 ${top_dir}/Examples/CD59/CHARMMff/ProtG.pdb) <(tail -n +5 ${top_dir}/Examples/CD59/CHARMMff/ProtG.pdb) | grep -v "^AUTHOR" > ProtG_skip_header_reference.pdb

    diff ProtG_skip_header.pdb ProtG_skip_header_reference.pdb
    diff ProtG.itp "${top_dir}/Examples/CD59/CHARMMff/ProtG.itp"
)

rm -rf "${top_dir}/tmp/6_${test_variant}"
mkdir -p "${top_dir}/tmp/6_${test_variant}"

(
    # AMBER-FF

    cd "${top_dir}/tmp/6_${test_variant}"

    doglycans -f "${top_dir}/dat/${input_itp_file}" -p "${top_dir}/dat/${input_prep_file}" -s "${top_dir}/Examples/CD59/sequence_file.seq" -c "${top_dir}/Examples/CD59/AMBERff/ProtH.pdb" -i "${top_dir}/Examples/CD59/AMBERff/ProtH.itp" -o ProtG.pdb -t ProtG.itp -B --amber

    cat <(head -n 3 ProtG.pdb) <(tail -n +5 ProtG.pdb) | grep -v "^AUTHOR" > ProtG_skip_header.pdb
    cat <(head -n 3 ${top_dir}/Examples/CD59/AMBERff/ProtG.pdb) <(tail -n +5 ${top_dir}/Examples/CD59/AMBERff/ProtG.pdb) | grep -v "^AUTHOR" > ProtG_skip_header_reference.pdb

    diff ProtG_skip_header.pdb ProtG_skip_header_reference.pdb
    diff ProtG.itp "${top_dir}/Examples/CD59/AMBERff/ProtG_vanilla.itp"
)

#
# Building glycolipid head group from ceramide tail
#

rm -rf "${top_dir}/tmp/7_${test_variant}"
mkdir -p "${top_dir}/tmp/7_${test_variant}"

(
    # OPLS-AA

    cd "${top_dir}/tmp/7_${test_variant}"

    doglycans -W -f "${top_dir}/dat/${input_itp_file}" -p "${top_dir}/dat/${input_prep_file}" -s "${top_dir}/Examples/GM1/sequence_file.seq" -c "${top_dir}/Examples/GM1/OPLS_AAff/CER.pdb" -i "${top_dir}/Examples/GM1/OPLS_AAff/CER.itp" -o GLIP.pdb -t GLIP.itp -B --opls

    cat <(head -n 3 GLIP.pdb) <(tail -n +5 GLIP.pdb) | grep -v "^AUTHOR" > GLIP_skip_header.pdb

    if $(is_openbabel_v3) ; then
        cat <(head -n 3 ${top_dir}/Examples/GM1/OPLS_AAff/GLIP.pdb) <(tail -n +5 ${top_dir}/Examples/GM1/OPLS_AAff/GLIP.pdb) | grep -v "^AUTHOR" > GLIP_skip_header_reference.pdb
    else
        cat <(head -n 3 ${top_dir}/Examples/GM1/OPLS_AAff/GLIP.ob2.pdb) <(tail -n +5 ${top_dir}/Examples/GM1/OPLS_AAff/GLIP.ob2.pdb) | grep -v "^AUTHOR" > GLIP_skip_header_reference.pdb
    fi

    diff GLIP_skip_header.pdb GLIP_skip_header_reference.pdb

    diff GLIP.itp "${top_dir}/Examples/GM1/OPLS_AAff/GLIP.itp"
)

if $(is_openbabel_v3) ; then
    rm -rf "${top_dir}/tmp/8_${test_variant}"
    mkdir -p "${top_dir}/tmp/8_${test_variant}"

    (
        # CHARMM-FF

        cd "${top_dir}/tmp/8_${test_variant}"

        doglycans -W -f "${top_dir}/dat/${input_itp_file}" -p "${top_dir}/dat/${input_prep_file}" -s "${top_dir}/Examples/GM1/sequence_file.seq" -c "${top_dir}/Examples/GM1/CHARMMff/CER.pdb" -i "${top_dir}/Examples/GM1/CHARMMff/CER.itp" -o GLIP.pdb -t GLIP.itp -B --charmm ${top_dir}/dat/GLYCAM_06j_resname.txt

        cat <(head -n 3 GLIP.pdb) <(tail -n +5 GLIP.pdb) | grep -v "^AUTHOR" > GLIP_skip_header.pdb
        cat <(head -n 3 ${top_dir}/Examples/GM1/CHARMMff/GLIP.pdb) <(tail -n +5 ${top_dir}/Examples/GM1/CHARMMff/GLIP.pdb) | grep -v "^AUTHOR" > GLIP_skip_header_reference.pdb

        diff GLIP_skip_header.pdb GLIP_skip_header_reference.pdb
        diff GLIP.itp "${top_dir}/Examples/GM1/CHARMMff/GLIP.itp"
    )
fi

echo "All tests succeeded for ${test_variant}"
